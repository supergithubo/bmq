﻿<html>
<head>
<link rel="stylesheet" type="text/css" href="ii.css">
<title>
BMQ 24-25 San Carlos Finance Corporation | Client's Information
</title>
</head>
 

<body class="bg">
<div class=container>
        <div class=banner>
                <img src="BannerBMQ1.jpg">
        </div>
		
		
<body id="body-color">
        <div id="login">
<tr>
		<td align= "center"><font color="black">
		<form action="login.php" method="POST">
</tr>		
		
	<div id=menu-centered>
		<ul>
			<li><a href="list.php"> List of Clients </a></li>
            <li><a href="logout.php"> Logout</a></li>
		</ul>
    </div>
		</form>	

		
<form action="admin.php" method="GET" name="form1">	
<style>
table {
    width:58%;
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;
}
table#t01 tr:nth-child(even) {
    background-color: #eee;
}
table#t01 tr:nth-child(odd) {
   background-color:#fff;
}
table#t01 th	{
    background-color: black;
    color: white;
}
table#t02 th	{
    background-color: white;
    color: black;
}
</style>
</head>	

<center><br>

 <table id="t02">
  <tr>
    <th><center><H2>Client's Information</H2></th>
  </TR>
<table id="t01">
  <tr>
    <th><label for="lastname">Last Name</label></th>
    <td><input type="text" id="lastname" name="lastname" size="50"/></td>
 </tr>
   <tr>
    <th><label for="firstname">First Name</label></th>
    <td><input type="text" id="firstname" name="firstname" size="50"/></td>
 </tr>
     <tr>
    <th><label for="middlename">Middle Name</th>
    <td><input type="text" name="middlename" size="50"/></td>
  </tr>
   <tr>
    <th><label for="gender">Gender</label></th>
    <td><input id="gender" type="text" value="M" name="gender"/ size="50"></td>
 </tr>
  <tr>
    <th><label for="address">Address</label></th>		
    <td><input type="text" id="address" name="address" size="50"/></td>	    
  </tr>  
  <tr>
    <th><label for="daterelease">Date of Release</label></th>
    <td><input type="text" id="daterelease" name="daterelease" size="50"/></td>
  </tr>
  <tr>
    <th><label for="duedate">Due Date</label></th>
    <td><input type="text" id="duedate" name="duedate" size="50"/></td>
  </tr>
   <tr>
    <th><label for="amounts">Amount of Loan</label></th> 	
    <td><input id="amounts" type="text" name="amounts" size="50"></td>
  </tr>
  <tr>
    <th><label for="interest">Interest</label></th>
    <td><input id="interest" type="text" name="interest" size="47"> % </td>
  </tr>
	<tr>
	<th><label for="months">Month/s: </th>
	<td><input id="months" type="text" name="months" size="50"></td>
  <tr>
	<th><label for="modepayment">Mode of Payment</label></th>
    <td><input id="modepayment" type="text" value="Cash" name="modepayment" size="50"/></td>
  </tr>
  <tr>
    <th><label for="collector">Collector</label></th>
    <td><select id="collector" name="collector"> <option></option>
	<option>Michele Ibasan </option>
	<option>Rhea Aguirre</option>
	<option>Nette Bautista</option>
	</selected>
	</td>
  </tr>
   <tr>
    <th><label for="reloan">Reloan</label></th>
    <td><input id="reloan" type="text" value="Y" name="reloan" size="50"/></td>
  </tr>
      <tr>
    <th><label for="tpayment">Total Amount</th>
    <td><input id ="tpayment" name="tpayment" size="50"></td>
  </tr>
</table>

<br>
		<input name="submit" type="submit" value="Edit"/>
		<input name="submit" type="submit" value="Update"/>
		<input name="submit" type="submit" value="Delete"/>
</form>

		<br><br>
<div>
         <div class=footer>
                    <p align=center>
                           <p> <colspan=4 align=center>Copyright &copy 2015 All Rights Reserved | BMQ 24-25 Finance Corporation </p>
         </div>
</div>		 
</BODY>
</body>
</html>